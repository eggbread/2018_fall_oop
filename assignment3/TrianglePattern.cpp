#include "TrianglePattern.h"
#include <iostream>

using namespace std;

TrianglePattern::TrianglePattern(){
    height=0;
}
TrianglePattern::TrianglePattern(int x){
    height=x;
}
void TrianglePattern::set_height(int x){
    height=x;
}
int TrianglePattern::patternHelper(int h){
if(h<=0){
    return 0;
}else{
    for(int i=0;i<h;i++){
        cout<<get_pattern();
    }
    cout<<endl;
    return patternHelper(h-1);
}
}
void TrianglePattern::printPattern(){
    cout<<"The Right Triangle Pattern: ( height = "<<height<<" )"<<endl;
    patternHelper(height);
    cout<<endl<<endl;
}
