#include "Icecream.h"
#include <string>
#include <iostream>

using namespace std;

IceCream::IceCream(bool cone,int number,string dessertName):dessertItem(dessertName){
    setNumberOfScoops(number);
    setType(cone);
    setName(dessertName);
}
void IceCream:: setNumberOfScoops(int number){
    numberOfScoops=number;
}
double IceCream:: getNumberOfScoops(){
    return numberOfScoops;
}
void IceCream:: setType(bool cone){
    isCone=cone;
}
bool IceCream:: getType(){
    return isCone;
}
void IceCream:: print(){
    cout<<getName()<<endl<<getNumberOfScoops()<<" scoops"<<endl;
}
double IceCream:: getCost(){
    if(getType()==true){
        return getNumberOfScoops()*1+0.5;
    }else{
        return getNumberOfScoops();
    }
}