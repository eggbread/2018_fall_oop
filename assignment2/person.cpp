#include "person.h"
#include <string>
#include <iostream>

using namespace std;


person::person(string name,int age){
    setProfession(name);
    setAge(age);
}
void person::setProfession(string name){
    profession=name;
}
void person::setAge(int year){
    age=year;
}
string person::getProfession(){
    return profession;
}
int person::getAge(){
    return age;
}
void person::display(){
    cout<<"My profession is: "<<getProfession()<<endl;
    cout<<"My age is: "<<endl;
    walk();
    talk();
}
void person::walk(){
    cout<<"I can walk."<<endl;
}
void person::talk(){
    cout<<"I can talk."<<endl;
}