#include <iostream>
#include <string>
#include "footballer.h"

using namespace std;

footballer::footballer(string name,int age):person(name,age){
    setProfession(name);
    setAge(age);
}
void footballer::playFootball(){
    cout<<"I can play Football."<<endl;
}